package com.company;

import java.util.Date;

public class Student {
    int id, course;
    String name, Lname, Mname;
    String Bday;
    String adress, phone, facult, group;

    Student(int id, String name,String Lname, String Mname, String Bday, String adress, String phone, String facult, int course, String group) {
        this.id=id;
        this.name=name;
        this.Lname=Lname;
        this.Mname=Mname;
        this.Bday=Bday;
        this.adress=adress;
        this.phone=phone;
        this.facult=facult;
        this.course=course;
        this.group=group;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String lname) {
        this.Lname = lname;
    }

    public void setPatronymic(String mname) {
        this.Mname = mname;
    }

    public void setBorn(String bday) {
        this.Bday = bday;
    }

    public void setAddress(String adress) {
        this.adress = adress;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFaculty(String facult) {
        this.facult = facult;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }
    public String getLname() {
        return name;
    }
    public String getName() {
        return Lname;
    }
    public String getMname() {
        return Mname;
    }
    public String getBday() {
        return Bday;
    }
    public String getAdress() {
        return adress;
    }
    public String getPhone() {
        return phone;
    }
    public String getFacult() {
        return facult;
    }
    public int getCourse() {
        return course;
    }
    public String getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return "Студент " + "id=" + id + ", Фамилия=" + Lname + ", Имя=" + name + ", Отчество=" + Mname + ", Дата рождения=" + Bday + ", Адрес=" + adress + ", Телефон=" + phone + ", Факультет=" + facult + ", группа=" + group + ", курс=" + course;
    }



}
