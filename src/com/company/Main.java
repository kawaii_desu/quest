package com.company;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
	    Student arrayStudents[] = new Student[3];
	    arrayStudents[0] = new Student(1, "Валерий","Жмышенко","Альбертович", "23.03.1998","Чкалова 3","+78005550505","Педагогический",4,"14П");
        arrayStudents[1] = new Student(2, "Василий","Васичкин","Васильич", "25.01.1997","Тимирязево 4","+77773343033","Педагогический",5,"13П");
        arrayStudents[2] = new Student(3, "Валерий","Требушет","Валеревич", "01.10.1998","Одесская 148","+79058403775","Аэрокосмический",4,"14АК");
        Scanner in = new Scanner(System.in);
        System.out.println("Введите факультет: 1 педагогический, 2 аерокосмический");
        int facult = scan.nextInt();
            switch (facult) {
                case 1:
                    for (Student student : arrayStudents) {
                        if (student.getFacult().equals("Педагогический")) {
                            System.out.println(student.toString());
                        }
                    }
                    break;
                case 2:
                    for (Student student : arrayStudents) {
                        if (student.getFacult().equals("Аэрокосмический")) {
                            System.out.println(student.toString());
                        }
                    }
                    break;
                default:
                    System.out.println("Неверный номер");
            }

                System.out.println("Педагогический:");
                for (Student student : arrayStudents) {
                    if (student.getFacult().equals("Педагогический")) {
                        System.out.println(student.toString());
                    }
                }
                System.out.println("Аэрокосмический:");
                for (Student student : arrayStudents) {
                    if (student.getFacult().equals("Аэрокосмический")) {
                        System.out.println(student.toString());
                    }
                }

                System.out.println("Студенты 4 курса:");
                for (Student student : arrayStudents) {
                    if (student.getCourse()==4) {
                        System.out.println(student.toString());
                    }
                }
                System.out.println("Студенты 5 курса:");
                for (Student student : arrayStudents) {
                    if (student.getCourse()==5) {
                        System.out.println(student.toString());
                    }
                }

        System.out.println("Выберите учебную группу (14П, 13П, 14АК:");
        String group;
        group = scan.next();

        switch (group.toLowerCase()) {
            case "14П": for (Student student : arrayStudents) {
                if (student.getGroup().equals("14П")) {
                    System.out.println(student.toString());
                }
            }
                break;
            case "13П": for (Student student : arrayStudents) {
                if (student.getGroup().equals("13П")) {
                    System.out.println(student.toString());
                }
            }
                break;
            case "14АК": for (Student student : arrayStudents) {
                if (student.getGroup().equals("14АК")) {
                    System.out.println(student.toString());
                }
            }
                break;
            default: System.out.println("Неверно");
        }
            }
    }

